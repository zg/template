{
  description = "niri";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }: {
    packages.x86_64-linux.default =
      with import nixpkgs { system = "x86_64-linux"; };

      rustPlatform.buildRustPackage rec {
        pname = "niri";
        version = "unstable-2023-10-31";

        src = fetchFromGitHub {
          owner = "YaLTeR";
          repo = "niri";
          rev = "d854c2d699b15c68c4715dc6be803065c01f2fe6";
          hash = "sha256-QYH3sG1TKJbKBeZdI9FtmJuY5DFmMdOJviYPrPK8FHo=";
        };

        cargoLock = {
          lockFile = ./Cargo.lock;
          outputHashes = {
            "smithay-0.3.0" = "sha256-cRBJ8r2fQ8d97DADOxfmUF5JYcOHQ05u8tMhVXmbrbE=";
          };
        };

        nativeBuildInputs = [
          pkg-config
          rustPlatform.bindgenHook
        ];

        buildInputs = [
          libxkbcommon
          pipewire
          seatd
          udev
          wayland
          libinput
          mesa # libgbm
          xwayland
          xorg.libX11
          xorg.libXcursor
          xorg.libXi
          libglvnd
        ];
        
        postInstall = ''
          export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"${libglvnd}/lib"; 
        '';

        meta = with lib; {
          description = "A scrollable-tiling Wayland compositor";
          homepage = "https://github.com/YaLTeR/niri";
          license = licenses.gpl3Only;
          maintainers = with maintainers; [ iogamaster ];
          mainProgram = "niri";
          inherit (wayland.meta) platforms;
        };
      };
  };
}
