{ pkgs ? import <nixpkgs> { } }:

# how to find all not found libs about a bin "foo"
# ldd foo | grep  'not  found'

# how to print the interpreter about a bin "foo"
# patchelf --print-interpreter foo

# how to prrint all needed libs about a bin "foo"
# patchelf --print-needed foo

# all not found libs
let
  libPath = pkgs.lib.makeLibraryPath [ pkgs.sqlite ];
  exec = "foo";
in
pkgs.mkShell {
  name = "worked";
  shellHook = ''
    patchelf \
    --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
    # --set-rpath "${libPath}" \
    --add-rpath "${libPath}" \
    ${exec}
  '';
}
