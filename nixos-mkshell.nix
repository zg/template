{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  # shell  name
  name = "my-shell";
  # add  pkgs can access in this shell env
  packages = [ pkgs.gcc pkgs.python ];
  # add  build dependencies
  inputsFrom = [ pkgs.hello pkgs.gnutar ];
  # some bash shell statements can be executed by nix-shell
  shellHook = ''
    export MY_VAR=foo
  '';
}
